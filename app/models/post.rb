class Post < ActiveRecord::Base
  
  # comments belong to a post and if we delete a post
  # we should delete all of its associated comments as well 
  has_many :comments, dependent: :destroy

  # ensure a post has a title and body before saving to database
  validates_presence_of :title
  validates_presence_of :body

end
