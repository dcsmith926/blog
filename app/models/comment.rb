class Comment < ActiveRecord::Base
  
  # a comment belongs to a post
  belongs_to :post

  # ensure a comment has an associated post_id
  # as well as a body before saving it to the database
  validates_presence_of :post_id
  validates_presence_of :body
  
end
